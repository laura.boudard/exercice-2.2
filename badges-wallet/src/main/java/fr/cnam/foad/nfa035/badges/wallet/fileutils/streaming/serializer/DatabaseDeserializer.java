package fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.serializer;

import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.media.ImageFrameMedia;
import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.media.ResumableImageFrameMedia;
import org.apache.commons.codec.binary.Base64InputStream;

import java.io.*;

public interface DatabaseDeserializer<M extends ImageFrameMedia> extends BadgeDeserializer<M> {

    <K extends InputStream> K getDeserializingStream(String data) throws IOException;


    @Override
    public void deserialize(ResumableImageFrameMedia media) throws IOException {
        BufferedReader br = media.getEncodedImageReader(true);
        String[] data = br.readLine().split(";");
        try (OutputStream os = getSourceOutputStream()) {
            getDeserializingStream(data[2]).transferTo(os);
        }
    }

    @Override
    public InputStream getDeserializingStream(String data) throws IOException {
        return new Base64InputStream(new ByteArrayInputStream(data.getBytes()));
    }


}


