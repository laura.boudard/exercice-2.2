package fr.cnam.foad.nfa035.badges.wallet.dao.impl;

import fr.cnam.foad.nfa035.badges.wallet.dao.BadgeWalletDAO;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.IOException;

public class MultiBadgeWalletDAOImpl extends BadgeWalletDAO {

    private static final Logger LOG = LogManager.getLogger(BadgeWalletDAO.class);

    private File walletDatabase;


    public MultiBadgeWalletDAOImpl(String dbPath, File walletDatabase) throws IOException {
        super(dbPath);
        this.walletDatabase = walletDatabase;
    }

    public MultiBadgeWalletDAOImpl(String dbPath) throws IOException {
        super(dbPath);
    }
}
