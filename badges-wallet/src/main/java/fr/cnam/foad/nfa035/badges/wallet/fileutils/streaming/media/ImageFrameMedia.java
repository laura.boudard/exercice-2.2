package fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.media;

import java.io.*;

public interface ImageFrameMedia<T> {

   T getChannel();


    /** créé un flux de sortie de l'image encodée
     * @return
     * @throws IOException
     */
     OutputStream getEncodedImageOutput() throws IOException;




}

