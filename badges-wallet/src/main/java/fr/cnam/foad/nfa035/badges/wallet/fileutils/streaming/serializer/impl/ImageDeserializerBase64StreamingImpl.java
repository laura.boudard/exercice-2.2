package fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.serializer.impl;

import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.media.AbstractImageFrameMedia;
import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.media.ImageFrameMedia;
import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.serializer.AbstractStreamingImageDeserializer;
import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.serializer.DatabaseDeserializer;
import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.serializer.ImageStreamingDeserializer;
import org.apache.commons.codec.binary.Base64InputStream;

import java.io.*;

/**
 * Implémentation Base64 de désérialiseur d'image, basée sur des flux.
 */
public class ImageDeserializerBase64StreamingImpl extends AbstractStreamingImageDeserializer<AbstractImageFrameMedia> {

    private OutputStream sourceOutputStream;



    /**
     * Constructeur élémentaire
     * @param sourceOutputStream
     */
    public ImageDeserializerBase64StreamingImpl(OutputStream sourceOutputStream) {
        this.sourceOutputStream = sourceOutputStream;
    }

    /**
     * {@inheritDoc}
     *
     * @param media
     * @return le flux de lecture
     * @throws IOException
     */
    @Override
    public InputStream getDeserializingStream(AbstractImageFrameMedia media) throws IOException {
        return new Base64InputStream(media.getEncodedImageInput());
    }


    /**
     * @return
     */
    @Override
    public <T extends OutputStream> void setSourceOutputStream(T os) {
    }
}
