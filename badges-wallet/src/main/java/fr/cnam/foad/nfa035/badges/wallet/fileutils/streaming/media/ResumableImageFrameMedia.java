package fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.media;

import java.io.BufferedReader;
import java.io.IOException;

public interface ResumableImageFrameMedia<T> extends ImageFrameMedia<T> {


    /** Sert à lire le texte à partir d'un flux en lecture de caractère, en mettant les caractères en mémoire tampon
     * afin de permettre une lecture efficace des caractères
     * @param resume boolean indiquant
     * @return
     * @throws IOException
     */
    BufferedReader getEncodedImageReader(boolean resume) throws IOException;


}
