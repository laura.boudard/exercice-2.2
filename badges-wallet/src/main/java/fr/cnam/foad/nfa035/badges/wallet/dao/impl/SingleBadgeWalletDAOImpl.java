package fr.cnam.foad.nfa035.badges.wallet.dao.impl;

import fr.cnam.foad.nfa035.badges.wallet.dao.BadgeWalletDAO;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;

public class SingleBadgeWalletDAOImpl {

    private static final Logger LOG = LogManager.getLogger(BadgeWalletDAO.class);

    private File walletDatabase;

    public SingleBadgeWalletDAOImpl(File walletDatabase) {
        this.walletDatabase = walletDatabase;
    }

}
