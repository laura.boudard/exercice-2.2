package fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.serializer;

import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.media.ImageFrameMedia;

import java.io.IOException;
import java.io.OutputStream;

public interface BadgeDeserializer<M extends ImageFrameMedia> {
    /**
     * @param media
     * @throws IOException
     */
    void deserialize(M media) throws IOException;

    /**
     * @param <T>
     * @return
     */
    <T extends OutputStream> T getSourceOutputStream();

    /**
     * @param os
     * @param <T>
     */
    <T extends OutputStream> void setSourceOutputStream(T os);
}